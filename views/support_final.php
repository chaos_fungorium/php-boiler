<div id="menu">
  <div id="menu_back"></div>
  <a class="ex_close" onclick="toggleMenu()"><i class="fa fa-times"></i></a>
  <div class="social">
    <a href="https://www.facebook.com/vagabond.vege" target="_blank"><i class="fab fa-facebook-square"></i></a>
    <a href="https://www.instagram.com/vagabond.vege/" target="_blank"><i class="fab fa-instagram"></i></a>
  </div>
  <nav>
    <ul>
      <li><a href="/">Home</a></li>
      <li><a href="/support">Support Us</a></li>
      <li><a href="/about">About Us</a></li>
      <li><a href="/produce">Produce</a></li>
      <li><a href="/#contact">Contact</a></li>
    </ul>
  </nav>
</div>

<header>
  <div class="icon">
    <img src="https://cdn.vagabondvege.nz/images/logo/logo_sm.png" alt="vagabond vege logo. Linocut vegetables in olive.">
  </div>
  <div class="title">
    <h1>Support the Vagabonds</h1>
  </div>
  <ul class="menu mobile">
    <li onclick="toggleMenu()"><a><i class="fa fa-bars"></i></a></li>
  </ul>
  <ul class="menu">
    <li><a href="/">Home</a></li>
    <li><a href="/support">Support Us</a></li>
    <li><a href="/about">About Us</a></li>
    <li><a href="/produce">Produce</a></li>
    <li><a href="/#contact">Contact</a></li>
    <div class="social_icons">
      <a href="https://www.facebook.com/vagabond.vege" target="_blank"><i class="fab fa-facebook-square"></i></a>
      <a href="https://www.instagram.com/vagabond.vege/" target="_blank"><i class="fab fa-instagram"></i></a>
    </div>
  </ul>
</header>

<div class="content mustard header-margin">
  <div class="container">
    <div style="padding: 26px 0px 20px 0px; margin: 32px 0px 0px 0px; background: #f5e8ce; text-align: center;">
      <h3 class="no-bm" style="color: #f1a841;"><?php echo $total_donations; ?> people invested $<?php echo $total_donation; ?></h3>
    </div>
    <br/>
    <br/>

    <h4 class="banner white">We sought crowdfunding support through September to help with the start-up costs and challenges of our first growing season. The support we then received far exceeded what any of us could have imagined.</h4>
    <h4 class="banner white">This process has meant so much more than simply financial support for the infrastructure we need. Knowing that there is a community of people that believe in us, who believe in this vision of small scale regenerative farming and are now connected to this place, means that we have the ability to move forward and grow with greater strength.</h4>
    <h4 class="banner white">Thank you to all those who supported through spreading the word and sending us kind words of encouragement, this means the world to us.</h4>
    <h4 class="banner white">To us, sustainability equals working with others and knowing we are backed by our community. You’re on this journey with us, watch us grow!</h4>
  </div>
</div>

<div id="perks" class="content olive">
  <div class="container">
    <h2 class="banner white">A massive thanks to all our backers!</h2>

    <div id="OPTION_RAFFLE" class="option">
      <div class="row">
        <div class="four columns">
          <div class="option-img">
            <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="https://cdn.vagabondvege.nz/images/graphic/FULL_FIELD.png" alt="">
          </div>
        </div>
        <div class="eight columns">
          <h4 class="option-title">Be in to win! <span class="option-price">$30</span></h4>
          <p class="option-description">Who doesn't like a raffle! Get your name in the box to win a box of goodies, your contribution will go towards the big infrastructure on the farm.</p>
          <button type="button" class="hollow olive"><?php echo $donated['RAFFLE'] ?> people entered the raffle</button>
        </div>
      </div>
    </div>

    <div id="OPTION_TREE" class="option">
      <div class="row">
        <div class="four columns">
          <div class="option-img">
            <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="https://cdn.vagabondvege.nz/images/graphic/TREES.png" alt="">
          </div>
        </div>
        <div class="eight columns">
          <h4 class="option-title">Adopt-a-Tree <span class="option-price">$50</span></h4>
          <p class="option-description">Blow me down, what a gust! We need a shelterbelt to protect our farm from the wild westerly and chilly southerly. Your contribution will go towards planting a green belt of trees and shrubs, made up of natives and exotics. Wind protection, bird homes, bee food, and food forest all in one!</p>
          <button type="button" class="hollow olive"><?php echo $donated['TREE'] ?> people adopted a tree</button>
        </div>
      </div>
    </div>

    <div id="OPTION_MINI_CHILLER" class="option">
      <div class="row">
        <div class="four columns">
          <div class="option-img">
            <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="https://cdn.vagabondvege.nz/images/graphic/CHILLER_S.png" alt="">
          </div>
        </div>
        <div class="eight columns">
          <h4 class="option-title">Adopt-the-Chiller <span class="option-price">$100</span></h4>
          <p class="option-description">Veggies need to chill out! We are buying/building a chiller to keep your produce fresh. Your contribution will go towards the chiller, the concrete pad for the chiller to sit on, and a structure protecting it from the elements.</p>
          <button type="button" class="hollow olive"><?php echo $donated['MINI_CHILLER'] ?> people contributed to the chiller</button>
        </div>
      </div>
    </div>

    <div id="OPTION_SINGLE_BED" class="option">
      <div class="row">
        <div class="four columns">
          <div class="option-img">
            <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="https://cdn.vagabondvege.nz/images/graphic/SINGLE.png" alt="">
          </div>
        </div>
        <div class="eight columns">
          <h4 class="option-title">Adopt-a-Bed <span class="option-price">$150</span></h4>
          <p class="option-description">The farm’s building blocks, the humble vegetable bed! Have peace of mind knowing that there is a soily spot growing goodness in your honour. With diversity comes strength and many hands make light work - so why not be a bed adopter and help us get off (or in?!) the ground.</p>

          <button type="button" class="hollow olive"><?php echo $donated['SINGLE_BED'] ?> people adopted a bed</button>
        </div>
      </div>
    </div>

    <div id="OPTION_WASH_PACK" class="option">
      <div class="row">
        <div class="four columns">
          <div class="option-img">
            <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="https://cdn.vagabondvege.nz/images/graphic/WASH_PACK.png" alt="">
          </div>
        </div>
        <div class="eight columns">
          <h4 class="option-title">Adopt-the-Wash/Pack <span class="option-price">$250</span></h4>
          <p class="option-description">Quick, the van is pulling up! Would you give us a hand with the clean up?! Your contribution will go towards the processing facilities we need. Help the vege get clean and all dressed up for markets and deliveries!</p>

          <button type="button" class="hollow olive"><?php echo $donated['WASH_PACK'] ?> people contributed to the wash/pack</button>
        </div>
      </div>
    </div>

    <div id="OPTION_DOUBLE_BED" class="option">
      <div class="row">
        <div class="four columns">
          <div class="option-img">
            <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="https://cdn.vagabondvege.nz/images/graphic/DOUBLE.png" alt="">
          </div>
        </div>
        <div class="eight columns">
          <h4 class="option-title">Adopt-Two-Beds <span class="option-price">$300</span></h4>
          <p class="option-description">Cute! Snuggle up in your double bed! Get your beds side by side and have them grow together for ever more, bound together in delicious matrimony. Want to show someone you love them? What says ‘I appreciate you’ more than two plots of soil growing life together! Just think of those mycorrhizal fungi intermingling down below...</p>
          <button type="button" class="hollow olive"><?php echo $donated['DOUBLE_BED'] ?> people adopted a double bed</button>
        </div>
      </div>
    </div>

    <div id="OPTION_FAMILY_BLOCK" class="option">
      <div class="row">
        <div class="four columns">
          <div class="option-img">
            <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="https://cdn.vagabondvege.nz/images/graphic/FAMILY.png" alt="">
          </div>
        </div>
        <div class="eight columns">
          <h4 class="option-title">Adopt-a-Family-Block <span class="option-price">$500</span></h4>
          <p class="option-description">Want to know your family has a spot in the country? Whatever the make up of your family group, know that there is a bed here for each of you! With roots chatting away underground, here is something for the whole family.</p>

          <button type="button" class="hollow olive"><?php echo $donated['FAMILY_BLOCK'] ?> people adopted a family block</button>
        </div>
      </div>
    </div>

    <div id="OPTION_BLOCK_BREAK" class="option">
      <div class="row">
        <div class="four columns">
          <div class="option-img">
            <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="https://cdn.vagabondvege.nz/images/graphic/BLOCK_BREAK.png" alt="">
          </div>
        </div>
        <div class="eight columns">
          <h4 class="option-title">Adopt-a-Block-Break<span class="option-price">$1000</span></h4>
          <p class="option-description">This place is windy! We need windbreaks to protect our beds from the strong prevailing wind. Would you like to contribute to windbreaks between our beds? We need 6 breaks going up on the main production fields. Your contribution will go towards the materials needed for one of these breaks, and a selection of shrubs for planting shelterbelts.</p>

          <button type="button" class="hollow olive"><?php echo $donated['BLOCK_BREAK'] ?> people adopted a block break</button>
        </div>
      </div>
    </div>

    <div id="OPTION_CHILLER" class="option">
      <div class="row">
        <div class="four columns">
          <div class="option-img">
            <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="https://cdn.vagabondvege.nz/images/graphic/CHILLER.png" alt="">
          </div>
        </div>
        <div class="eight columns">
          <h4 class="option-title">Adopt-the-Chiller <span class="option-price">$1000</span></h4>
          <p class="option-description">Veggies need to chill out! We are buying/building a chiller to keep your produce fresh. Your contribution will go towards the chiller, the concrete pad for the chiller to sit on, and a structure protecting it from the elements.</p>

          <button type="button" class="hollow olive"><?php echo $donated['CHILLER'] ?> people contributed to the chiller</button>
        </div>
      </div>
    </div>

    <div id="OPTION_TRACTOR" class="option">
      <div class="row">
        <div class="four columns">
          <div class="option-img">
            <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="https://cdn.vagabondvege.nz/images/graphic/TRACTOR.png" alt="">
          </div>
        </div>
        <div class="eight columns">
          <h4 class="option-title">Adopt-the-Tractor <span class="option-price">$5000</span></h4>
          <p class="option-description">Help us with our heavy lifting! This human scale farm needs a little mechanical help from time to time - mowing, moving compost, rocks and wood chip etc. Fulfil your childhood dream and ours... support us with a tractor!</p>

          <button type="button" class="hollow olive"><?php echo $donated['TRACTOR'] ?> person adopted the tractor</button>
        </div>
      </div>
    </div>

    <div id="OPTION_TRACTOR_UPGRADE" class="option">
      <div class="row">
        <div class="four columns">
          <div class="option-img">
            <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="https://cdn.vagabondvege.nz/images/graphic/TRACTOR.png" alt="">
          </div>
        </div>
        <div class="eight columns">
          <h4 class="option-title">Upgrade-Danny-the-Tractor <span class="option-price">$4000</span></h4>
          <p class="option-description">The tractor is named, but now it needs personality - add a personalised feature of your choice. Help us make Danny the tractor the best tractor for the job! With your help we can get a fit for purpose tractor with all the implements we need. Fulfil your childhood dream and ours... support our tractor!</p>

          <button type="button" class="hollow olive"><?php echo $donated['TRACTOR_UPGRADE'] ?> person upgraded the tractor</button>
        </div>
      </div>
    </div>

    <div id="OPTION_HALF_TUNNEL" class="option">
      <div class="row">
        <div class="four columns">
          <div class="option-img">
            <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="https://cdn.vagabondvege.nz/images/graphic/HALF_TUNNEL.png" alt="">
          </div>
        </div>
        <div class="eight columns">
          <h4 class="option-title">Adopt-the-Seed-House <span class="option-price">$7000</span></h4>
          <p class="option-description">For the calm, nurturing individual who perhaps enjoys quiet music and slow walks in the bush, the propagation tunnel is for you! It’s the seeds first entry into the world, where they receive tender care to grow strong before they go out into the beds. Give the plants the growth they deserve and support us with half a propagation tunnel!</p>

          <button type="button" class="hollow olive">1 family adopted the seed nursery</button>
        </div>
      </div>
    </div>

    <div id="OPTION_WHOLE_TUNNEL" class="option">
      <div class="row">
        <div class="four columns">
          <div class="option-img">
            <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="https://cdn.vagabondvege.nz/images/graphic/WHOLE_TUNNEL.png" alt="">
          </div>
        </div>
        <div class="eight columns">
          <h4 class="option-title">Adopt-the-Tunnel <span class="option-price">$7000</span></h4>
          <p class="option-description">For the party person, the growing tunnel is filled with heat and abundance; think tomatoes, eggplants and capsicum pumping with fruit! Give the plants the party they deserve and support us with a whole growing tunnel.</p>
          <button type="button" class="hollow olive">1 family adopted the growing tunnel</button>
        </div>
      </div>
    </div>

    <p class="white">* Vegetables perks are only deliverable to the Wellington/Wairarapa region, or by direct pick-up from the farm. For other regions your vegetables will be replaced by a non-perishable perk</p>
  </div>
</div>

<div id="breakdown" class="content white">
  <div class="container">
    <h4 class="banner olive">To date we have spent around $35,000 on items such as compost, propagation equipment, seeds, infrastructure, woodchip, irrigation, insurance, tools and leasing the land. Below are the remaining minimum start-up costs.</h4>

    <ol>
      <li class="olive">Commercial tunnel houses ($14,000)</li>
      <li class="olive">Commercial chiller ($8000)</li>
      <li class="olive">Compost for permanent beds ($7000)</li>
      <li class="olive">Building a wash/pack facility ($7000)</li>
      <li class="olive">Installing & planting windbreaks ($6000)</li>
      <li class="olive">Planting Shelterbelts ($2500)</li>
      <li class="olive">Small second hand tractor ($10,000)</li>
    </ol>

    <p class="olive"><b>Stretch Goal:</b> $80,000 covers increased costs within our wash/pack processing facilities and the purchase of holding tanks for water resilience. A large investor has pushed us past our ultimate goal and allowed us to add a stretch goal to cover increased costs!</p>
    <p class="olive"><b>Ideal Goal:</b> $60,000 matches our personal investment in the farm. This gives us full financial stability and allows us to invest in the quality infrastructure we need now. Reaching our ultimate goal allows us to spend more time focussed on growing our farm, setting up our CSA membership and supporting more positive change within our food systems.</p>
    <p class="olive"><b>Minimum Goal:</b> $40,000 allows us to purchase the basics for our farm set up. Our minimum goal means we may have to wait until we have a steady income stream in order to invest in the quality infrastructure we need now.</p>

  </div>
</div>

<footer>
  <h5>Vagabond Vege, Monty's Lane, Te Hupenui, Greytown</h5>
  <h5><a href="https://www.facebook.com/vagabond.vege" target="_blannk">Facebook</a><span>|</span><a href="https://www.instagram.com/vagabond.vege" target="_blank">Instagram</a></h5>
</footer>
