<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="assets/js/admin.js"></script>

<script type="text/javascript">
  window.toSendIds = <?php echo $ns_ids; ?>;
</script>

<header>
  <div class="icon">
    <img src="https://cdn.vagabondvege.nz/images/logo/logo_lg.png" alt="vagabond vege logo. Linocut vegetables in olive.">
  </div>
  <div class="title">
    <h1>VAG HQ</h1>
  </div>
  <ul class="menu">
    <li><a href="/download">Download</a></li>
    <li><a href="/logout">Logout</a></li>
  </ul>
</header>

<div class="content olive">
  <div class="container">
    <h4 class="banner white">Donations.</h4>

    <table class="u-full-width">
      <thead>
        <tr>
          <th>Name</th>
          <th>Code</th>
          <th>Sent</th>
          <th>Finished</th>
          <th>Options</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($follow_ups as $index => $row) { ?>
          <tr>
            <td><?php echo $row['name'] ?></td>
            <td><?php echo $row['access'] ?></td>
            <td><i class="fad <?php echo $row['hasSent'] ? 'fa-check-square' : 'fa-times-square'; ?>"></i></td>
            <td><i class="fad <?php echo $row['hasResponded'] ? 'fa-check-square' : 'fa-times-square'; ?>"></i></td>
            <td><?php echo $row['options'] ?></td>

            <td><?php echo $row['response'] ?></td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>

<div class="content mustard">
  <div class="container">
    <h4 class="banner white">Send Emails</h4>

    <button id="sendButton" class="button filled olive" onclick="sendEmails()">Send Emails</button>
    <div id="progress" style="text-align: center; display: none;">
      <i class="fas fa-2x fa-spinner fa-pulse"></i>
      <br/>
      <p>Progress: <span id="finished">0</span> of <span id="total">100</span></p>
    </div>
  </div>
</div>
