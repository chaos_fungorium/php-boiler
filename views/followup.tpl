<!DOCTYPE html>
<html lang="en" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
   <head>
      <title></title>
      <meta charset="utf-8"/>
      <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
			<link rel="stylesheet" href="https://vagabond-cdn.s3.ap-southeast-2.amazonaws.com/src/email.css">
      <!--[if mso]>
      <xml>
         <o:OfficeDocumentSettings>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            <o:AllowPNG/>
         </o:OfficeDocumentSettings>
      </xml>
      <![endif]-->
      <style>
         * {
         box-sizing: border-box;
         }
         th.column {
         padding: 0
         }
         a[x-apple-data-detectors] {
         color: inherit !important;
         text-decoration: inherit !important;
         }
         #MessageViewBody a {
         color: inherit;
         text-decoration: none;
         }
         p {
         line-height: inherit
         }
         @media (max-width:700px) {
         .icons-inner {
         text-align: center;
         }
         .icons-inner td {
         margin: 0 auto;
         }
         .row-content {
         width: 100% !important;
         }
         .stack .column {
         width: 100%;
         display: block;
         }
         }
      </style>
   </head>
   <body style="background-color: #f5e8ce; margin: 0; padding: 0; -webkit-text-size-adjust: none; text-size-adjust: none;">
      <table border="0" cellpadding="0" cellspacing="0" class="nl-container" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #f5e8ce;" width="100%">
      <tbody>
         <tr>
            <td>
               <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-1" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #f5e8ce;" width="100%">
                  <tbody>
                     <tr>
                        <td>
                           <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="680">
                              <tbody>
                                 <tr>
                                    <th class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 5px;" width="100%">
                                       <table border="0" cellpadding="0" cellspacing="0" class="image_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                                          <tr>
                                             <td style="width:100%;padding-right:0px;padding-left:0px;padding-top:40px;">
                                                <div align="center" style="line-height:10px"><img src="https://cdn.vagabondvege.nz/images/logo/logo_sm.png" style="display: block; height: auto; border: 0; width: 238px; max-width: 100%;" width="238"/></div>
                                             </td>
                                          </tr>
                                       </table>
                                    </th>
                                 </tr>
                              </tbody>
                           </table>
                        </td>
                     </tr>
                  </tbody>
               </table>
               <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-2" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #f5e8ce;" width="100%">
                  <tbody>
                     <tr>
                        <td>
                           <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="680">
                              <tbody>
                                 <tr>
                                    <th class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 5px;" width="100%">
                                       <div class="spacer_block" style="height:40px;line-height:40px;"></div>
                                    </th>
                                 </tr>
                              </tbody>
                           </table>
                        </td>
                     </tr>
                  </tbody>
               </table>
               <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-3" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #f5e8ce;" width="100%">
                  <tbody>
                     <tr>
                        <td>
                           <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="680">
                              <tbody>
                                 <tr>
                                    <th class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 5px;" width="100%">
                                       <table border="0" cellpadding="10" cellspacing="0" class="heading_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                                          <tr>
                                             <td>
                                                <h1 style="margin: 0; color: #85975a; direction: ltr; font-family: 'Grenette Pro', Times, Beskerville, Georgia, serif; font-size: 31px; font-weight: normal; letter-spacing: normal; line-height: 120%; text-align: center; margin-top: 0; margin-bottom: 0;"><strong>Kia ora {$name}, thanks once again for supporting us!</strong></h1>
                                             </td>
                                          </tr>
                                       </table>
                                       <table border="0" cellpadding="10" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
                                          <tr>
                                             <td>
                                                <div style="font-family: sans-serif">
                                                   <div style="font-size: 14px; color: #5f6266; line-height: 1.5; font-family: 'Metric', Helvetica Neue, Helvetica, sans-serif;">
                                                      <p style="margin: 0; font-size: 16px; text-align: center; mso-line-height-alt: 24px;"><span style="font-size:16px;">You are one of 264 people who together invested $77,010 across one month of crowdfunding. Ng&#257; mihi nunui kia koe, thank you so much for your support! We are absolutely overwhelmed with your generosity. Your support means more than just financing the infrastructure we need. It means we are moving forth into this farm build, this kaupapa we follow, with strength in knowing that we are backed by a community of people who believe in us and understand the importance of small scale, regenerative and collaborative farming in Aotearoa.</span></p>
																											<br/>
                                                      <p style="margin: 0; font-size: 16px; text-align: center; mso-line-height-alt: 24px;"><span style="font-size:16px;">If you opted in to receive our newsletter, we will send these out at the end of each month. Otherwise, you can keep in the loop of our growing farm by following us on <a href="https://instagram/vagabond.vege" rel="noopener" style="text-decoration: underline; color: #8a3b8f;" target="_blank">instagram</a> or <a href="https://facebook.com/vagabond.vege" rel="noopener" style="text-decoration: underline; color: #8a3b8f;" target="_blank">facebook</a>. If you're in the area and would like to come visit, feel free to get in touch.</span></p>
                                                      <br/>
                                                      <p style="margin: 0; font-size: 16px; text-align: center; mso-line-height-alt: 24px;"><span style="font-size:16px;">We are very excited to have you here for our farm wh&#257;nau day! We are planning for a late summer gathering and will inform you of the date at the end of the year.</span></p>
                                                      <p style="margin: 0; font-size: 16px; text-align: center; mso-line-height-alt: 24px;"><br/><span style="font-size:16px;">Once again thank you so much for your support. Let's keep in touch!</span><br/><br/><span style="font-size:16px;">The Vagabonds,</span><br/><span style="font-size:16px;">Elle, Sheldon, Saskia &amp; Lise</span></p>
                                                   </div>
                                                </div>
                                             </td>
                                          </tr>
                                       </table>
                                       <table border="0" cellpadding="10" cellspacing="0" class="divider_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                                          <tr>
                                             <td>
                                                <div align="center">
                                                   <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                                                      <tr>
                                                         <td class="divider_inner" style="font-size: 1px; line-height: 1px; border-top: 1px solid #BBBBBB;"><span></span></td>
                                                      </tr>
                                                   </table>
                                                </div>
                                             </td>
                                          </tr>
                                       </table>
                                       <table border="0" cellpadding="0" cellspacing="0" class="heading_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                                          <tr>
                                             <td style="width:100%;text-align:center;">
                                                <h1 style="margin: 0; color: #85975a; font-size: 23px; font-family: 'Grenette Pro', Times, Beskerville, Georgia, serif; line-height: 120%; text-align: center; direction: ltr; font-weight: normal; letter-spacing: normal; margin-top: 0; margin-bottom: 0;"><strong>Let us know your perk details</strong></h1>
                                             </td>
                                          </tr>
                                       </table>
                                       <table border="0" cellpadding="10" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
                                          <tr>
                                             <td>
                                                <div style="font-family: sans-serif">
                                                   <div style="font-size: 14px; color: #5f6266; line-height: 1.5; font-family: 'Metric', Helvetica Neue, Helvetica, sans-serif;">
                                                      <p style="margin: 0; font-size: 14px; text-align: center; mso-line-height-alt: 22.5px;"><span style="font-size:15px;"><span style="font-size:16px;">If you backed us with a perk for naming something on the farm, or receiving some vegetables or merch, please follow the link below to let us know your address and names.</span><br/></span></p>
                                                   </div>
                                                </div>
                                             </td>
                                          </tr>
                                       </table>
                                       <table border="0" cellpadding="10" cellspacing="0" class="button_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                                          <tr>
                                             <td>
                                                <div align="center">
                                                   <a href="{$link}" style="text-decoration:none;display:inline-block;color:#ffffff;background-color:#f18063;border-radius:4px;width:auto;border-top:1px solid #F18063;border-right:1px solid #F18063;border-bottom:1px solid #F18063;border-left:1px solid #F18063;padding-top:5px;padding-bottom:5px;font-family:'Metric', Helvetica Neue, Helvetica, sans-serif;text-align:center;mso-border-alt:none;word-break:keep-all;" target="_blank"><span style="padding-left:35px;padding-right:35px;font-size:16px;display:inline-block;letter-spacing:normal;"><span style="font-size: 16px; line-height: 2; word-break: break-word; mso-line-height-alt: 32px;">View your perks and let us know the details</span></span></a>
                                                </div>
                                             </td>
                                          </tr>
                                       </table>
                                       <table border="0" cellpadding="10" cellspacing="0" class="divider_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                                          <tr>
                                             <td>
                                                <div align="center">
                                                   <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                                                      <tr>
                                                         <td class="divider_inner" style="font-size: 1px; line-height: 1px; border-top: 1px solid #BBBBBB;"><span></span></td>
                                                      </tr>
                                                   </table>
                                                </div>
                                             </td>
                                          </tr>
                                       </table>
                                       <table border="0" cellpadding="0" cellspacing="0" class="heading_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                                          <tr>
                                             <td style="width:100%;text-align:center;">
                                                <h1 style="margin: 0; color: #85975a; font-size: 23px; font-family: 'Grenette Pro', Times, Beskerville, Georgia, serif; line-height: 120%; text-align: center; direction: ltr; font-weight: normal; letter-spacing: normal; margin-top: 0; margin-bottom: 0;"><strong>What we have been doing</strong></h1>
                                             </td>
                                          </tr>
                                       </table>
                                       <table border="0" cellpadding="10" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
                                          <tr>
                                             <td>
                                                <div style="font-family: sans-serif">
                                                   <div style="font-size: 14px; text-align: center; color: #5f6266; line-height: 1.5; font-family: 'Metric', Helvetica Neue, Helvetica, sans-serif;">
                                                      <p style="margin: 0; mso-line-height-alt: 24px;"><span style="font-size:16px;">We have been very busy over the last week. Moving the last rocks from our first production area, harrowing it with the help of our friend Tom and his tractor, and forming the first beds! It's so great to have some beds filled with plants. So far we have beds filled with Kale, Broccoli, Cabbage, Chard, Coriander, Argugula, Mustard, Spinach, Potatoe, Zucchini and Tomatoes!</span></p>
                                                   </div>
                                                </div>
                                             </td>
                                          </tr>
                                       </table>
                                    </th>
                                 </tr>
                              </tbody>
                           </table>
                        </td>
                     </tr>
                  </tbody>
               </table>
               <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-4" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                  <tbody>
                     <tr>
                        <td>
                           <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="680">
                              <tbody>
                                 <tr>
                                    <th class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top;" width="50%">
                                       <table border="0" cellpadding="0" cellspacing="0" class="image_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                                          <tr>
                                             <td style="width:100%;padding-right:0px;padding-left:0px;padding-top:5px;padding-bottom:5px;">
                                                <div align="center" style="line-height:10px"><img src="https://cdn.vagabondvege.nz/images/pic/fu_1.jpg" style="display: block; height: auto; border: 0; width: 100%; max-width: 100%;"/></div>
                                             </td>
                                          </tr>
                                       </table>
                                    </th>
                                    <th class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top;" width="50%">
                                       <table border="0" cellpadding="0" cellspacing="0" class="image_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                                          <tr>
                                             <td style="width:100%;padding-right:0px;padding-left:0px;padding-top:5px;padding-bottom:5px;">
                                                <div align="center" style="line-height:10px"><img src="https://cdn.vagabondvege.nz/images/pic/fu_2.jpg" style="display: block; height: auto; border: 0; width: 100%; max-width: 100%;"/></div>
                                             </td>
                                          </tr>
                                       </table>
                                    </th>
                                 </tr>
                              </tbody>
                           </table>
                        </td>
                     </tr>
                  </tbody>
               </table>

               <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-6" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                  <tbody>
                     <tr>
                        <td>
                           <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="680">
                              <tbody>
                                 <tr>
                                    <th class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 5px;" width="100%">
                                       <div class="spacer_block" style="height:50px;line-height:50px;"></div>
                                    </th>
                                 </tr>
                              </tbody>
                           </table>
                        </td>
                     </tr>
                  </tbody>
               </table>
               <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-7" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                  <tbody>
                     <tr>
                        <td>
                           <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="680">
                              <tbody>
                                 <tr>
                                    <th class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 5px;" width="100%">
                                       <table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
                                          <tr>
                                             <td style="padding-left:10px;padding-right:10px;padding-top:10px;">
                                                <div style="font-family: sans-serif">
                                                   <div style="font-size: 14px; color: #85975a; line-height: 1.5; font-family: 'Metric', Helvetica Neue, Helvetica, sans-serif;">
                                                      <p style="margin: 0; font-size: 14px; text-align: center; mso-line-height-alt: 24px;"><span style="font-size:16px;">Interested in our produce?<br/></span></p>
                                                   </div>
                                                </div>
                                             </td>
                                          </tr>
                                       </table>
                                       <table border="0" cellpadding="10" cellspacing="0" class="heading_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                                          <tr>
                                             <td>
                                                <h1 style="margin: 0; color: #090823; direction: ltr; font-family: 'Metric', Helvetica Neue, Helvetica, sans-serif; font-size: 27px; font-weight: normal; letter-spacing: normal; line-height: 120%; text-align: center; margin-top: 0; margin-bottom: 0;"><strong>If you are in the Wairarapa area and you (or others you know) are interested in our CSA vege box subscription, please sign up here to be kept in the loop.</strong><br/></h1>
                                             </td>
                                          </tr>
                                       </table>
                                       <table border="0" cellpadding="10" cellspacing="0" class="button_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                                          <tr>
                                             <td>
                                                <div align="center">
                                                   <a href="https://www.vagabondvege.nz/produce" style="text-decoration:none;display:inline-block;color:#ffffff;background-color:#f18063;border-radius:4px;width:auto;border-top:1px solid #F18063;border-right:1px solid #F18063;border-bottom:1px solid #F18063;border-left:1px solid #F18063;padding-top:5px;padding-bottom:5px;font-family:'Metric', Helvetica Neue, Helvetica, sans-serif;text-align:center;mso-border-alt:none;word-break:keep-all;" target="_blank"><span style="padding-left:35px;padding-right:35px;font-size:16px;display:inline-block;letter-spacing:normal;"><span style="font-size: 16px; line-height: 2; word-break: break-word; mso-line-height-alt: 32px;">Join our CSA waitlist</span></span></a>
                                                </div>
                                             </td>
                                          </tr>
                                       </table>
                                    </th>
                                 </tr>
                              </tbody>
                           </table>
                        </td>
                     </tr>
                  </tbody>
               </table>
               <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-8" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                  <tbody>
                     <tr>
                        <td>
                           <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="680">
                              <tbody>
                                 <tr>
                                    <th class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 5px;" width="100%">
                                       <div class="spacer_block" style="height:50px;line-height:50px;"></div>
                                    </th>
                                 </tr>
                              </tbody>
                           </table>
                        </td>
                     </tr>
                  </tbody>
               </table>
               <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-9" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                  <tbody>
                     <tr>

                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      <!-- End -->
   </body>
</html>
