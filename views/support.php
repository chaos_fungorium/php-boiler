<!-- POPUP MENU MARKUP -->

<div id="menu">
  <div id="menu_back"></div>
  <a class="ex_close" onclick="toggleMenu()"><i class="fa fa-times"></i></a>
  <div class="social">
    <a href="https://www.facebook.com/" target="_blank"><i class="fab fa-facebook-square"></i></a>
    <a href="https://www.instagram.com/" target="_blank"><i class="fab fa-instagram"></i></a>
  </div>
  <nav>
    <ul>
      <li><a href="/">Home</a></li>
    </ul>
  </nav>
</div>

<!-- POPUP END -->

<!-- DONATE MODAL MARKUP -->

<div id="donateModal" class="modal">
  <div class="bg"></div>
  <div class="content white">
    <i class="fad fa-times" onclick="closeModal()"></i>
    <div id="formSuccess" style="display: none; padding: 80px 0px; text-align: center;">
      <h2 class="banner olive">Thank you for pledging your investment!</h2>
      <hr>
      <div class="row">
        <div class="six columns">
          <h4 class="banner olive">Pay by debit card online</h4>
          <p>Please use the link below to use Strip to securley transfer your investment with debit card</p>
          <!-- THIS LINK IS DYNAMICALLY CHANGED WHEN AN OPTION IS SELECTED -->
          <a id="STRIPE_LEAD_ON" class="button filled olive u-full-width" href="https://buy.stripe.com/test_28o8yO2NP8m9db23cc" target="_blank">Secure Stripe Portal</a>
        </div>
        <div class="six columns">
          <h4 class="banner olive">Pay by bank transfer</h4>
          <p>Please use the details below to directly pay your investment by bank transfer</p>
          <div class="detials" style="text-align: left; padding-left: 100px;">
            <p class="no-bm"><b>Bank Account Name:</b> **INSERT ACCOUNT NAME**</p>
            <p class="no-bm"><b>Account Number:</b> **INSERT ACCOUNT NUMBER**</p>
            <p class="no-bm"><b>Reference:</b> <i>**INSERT REFERENCE**</i></p>
            <p class="no-bm"><b>Particulars:</b> <i id="particular">YOUR NAME</i></p>
          </div>
        </div>
      </div>
      <br>
      <br>
      <p>If you opted to be updated about our progress or join our newletter we wil be in touch soon</p>
    </div>
    <div id="formLoader" style="display:none; padding: 200px 0px;">
      <div class="spinner">
        <i class="fad fa-5x fa-ring fa-spin"></i>
        <h4 class="banner mustard">Saving your details...</p>
      </div>
    </div>
    <div id="formPlatform">
      <h2 class="banner olive">Invest Now!</h2>
      <form id="donate_form" class="" action="/submit_donate" method="post">
        <div class="row">
          <div class="six columns">

            <div class="field">
              <label class="olive">Your Name</label>
              <input id="form_name" class="u-full-width" type="text" name="name" value="" placeholder="Name">
              <label class="warning">Valid Name Required</label>
            </div>
            <div class="field">
              <label class="olive">Your Email</label>
              <input id="form_email" class="u-full-width" type="text" name="email" value="" placeholder="Email">
              <label class="warning">Valid Email Required</label>
            </div>
            <div class="field">
              <label class="olive">Where in Aotearoa are you</label>
              <select class="u-full-width" name="region">
                <option value="Wairarapa">Wairarapa</option>
                <option value="Wellington">Wellington</option>
                <option value="Northland">Northland</option>
                <option value="Auckland">Auckland</option>
                <option value="Waikato">Waikato</option>
                <option value="BayofPlenty">Bay of Plenty</option>
                <option value="Gisborne">Gisborne</option>
                <option value="Hawkes Bay">Hawkes Bay</option>
                <option value="Taranaki">Taranaki</option>
                <option value="Manawatu-Whanganui">Manawatu-Whanganui</option>
                <option value="Tasman">Tasman</option>
                <option value="Nelson">Nelson</option>
                <option value="Marlbourogh">Marlbourogh</option>
                <option value="West Coast">West Coast</option>
                <option value="Canturbury">Canturbury</option>
                <option value="Otago">Otago</option>
                <option value="Southland">Southland</option>
                <option value="International">International</option>
              </select>
            </div>
            <label class="olive">
              <input type="checkbox" name="wants_updates" value="YES">
              <span class="label-body">Sign up the build updates</span>
            </label>
            <label class="olive">
              <input type="checkbox" name="wants_newsletter" value="YES">
              <span class="label-body">Sign up to our newsletter</span>
            </label>
          </div>
          <div class="six columns">
            <!-- THIS INFORMATION IS DYNAMICALLY CHANGED -->
            <h4 id="FINAL_TITLE" class="option-title">Adopt a bed: $150</h4>
            <p id="FINAL_DESCRIPTION">The farm’s building blocks, the humble vegetable bed! Have peace of mind knowing that there is a soily spot growing goodness in your honour. With diversity comes strength and many hands make light work - so why not be a bed adopter and help us get off (or in?!) the ground.</p>
            <input id="donate_id" type="text" name="donate_id" hidden>
            <input id="perk_name" type="text" name="perk_name" hidden>
            <input id="donate_option" type="text" name="donation_option" hidden>
            <input id="donate_dollars" type="number" name="donation_amount" hidden>
            <input id="donate_link" type="text" name="donate_link" hidden>
          </div>
          <button id="confirm" class="filled olive u-full-width" type="button" name="finish" onclick="submitForm()">Confirm your investment of $150</button>
          <p><i>After submitting this form you will be given payment options</i></p>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- DONATE MODAL END -->

<!-- HEADER BAR MARKUP -->

<header>
  <div class="icon">
    <img src="assets/placeholder.png" alt="vagabond vege logo. Linocut vegetables in olive.">
  </div>
  <div class="title">
    <h1>Support Kalmana</h1>
  </div>
  <ul class="menu mobile">
    <li onclick="toggleMenu()"><a><i class="fa fa-bars"></i></a></li>
  </ul>
  <ul class="menu">
    <div class="social_icons">
      <a href="https://www.facebook.com/" target="_blank"><i class="fab fa-facebook-square"></i></a>
      <a href="https://www.instagram.com/" target="_blank"><i class="fab fa-instagram"></i></a>
    </div>
  </ul>
</header>

<!-- HEADER BAR END -->

<!-- CONTENT START -->

<div class="content mustard header-margin">
  <div class="container">
    <h4 class="banner white">We realise we can't build this farm alone. To us, sustainability equals working with others and knowing we are backed by our community. We are seeking crowd funding to assist with start-up costs and challenges of our first growing season.</h4>
    <h4 class="banner white">If you back our vision and would like to contribute to the farm build please consider investing through one of the options listed below.</h4>
    <h4 class="banner white">Come on this journey with us!</h4>
  </div>
</div>

<!-- PAGE NAVIGATION -->

<div class="content white" style="padding: 0px 0px;">
  <div class="page_nav">
    <ul>
      <li><a href="#perks">Invest Now!</a></li>
      <li><a href="#">Navigation 1</a></li>
      <li><a href="#">Navigation 2</a></li>
      <li><a href="#breakdown">View Breakdown</a></li>
    </ul>
  </div>
</div>

<!-- PROGRESS INDICATOR -->

<div class="content olive">
  <div class="container" style="text-align: center;">
    <h4 class="banner white">Where we are at</h4>
    <h4 class="banner white"><span class="hl">$<?php echo $total_donation; ?></span> invested by <span class="highlight"><?php echo $total_donations; ?> people</span></h4>

    <!-- CHANGE THIS LOGIC BASED ON THE GOAL SELECTED -->
    <?php if ($total_donation < 60000) { ?>
    <div style="padding: 26px 0px 20px 0px; margin: 32px 0px 0px 0px; background: #f1a841">
      <h3 class="white no-bm">Help us reach our optimum target of $60,000</h3>
    </div>
    <?php } else { ?>
    <div style="padding: 26px 0px 20px 0px; margin: 32px 0px 0px 0px; background: #f1a841">
      <h3 class="white no-bm">Help us reach our stretch target of $80,000</h3>
      <h4 class="olive no-bm"><span class="inline-a"><a href="#breakdown">Read more about this</a></span></h4>
    </div>
    <?php } ?>

    <?php if ($total_donation < 60000) { ?>
    <div class="donation_meter">
      <div class="bg"></div>
      <div class="bg_bonus"></div>
      <div class="progress green" style="width: <?php echo $progress; ?>%"></div>
      <p class="percent" style="left: <?php echo $progress+1; ?>%;"><?php echo $percentage; ?>%</p>
      <p class="amount" style="left: calc(<?php echo $progress; ?>% - 7px);">$<?php echo $total_donation; ?></p>
      <p class="amount" style="right: calc(0% - 60px);">$60,000</p>
    </div>
    <?php } else { ?>
    <div class="donation_meter">
      <div class="bg"></div>
      <div class="bg_bonus"></div>
      <div class="progress green" style="width: <?php echo $progress; ?>%"></div>
      <p class="percent" style="left: <?php echo $progress+1; ?>%;"><?php echo $percentage; ?>%</p>
      <p class="amount" style="left: calc(<?php echo $progress; ?>% - 7px);">$<?php echo $total_donation; ?></p>
      <p class="amount" style="right: calc(0% - 60px);">$80,000</p>
    </div>
    <?php } ?>

    <!-- DYNAMICALLY UPDATES TIME UNTIL CLOSE -->
    <h4 class="banner white"><span id="TO_GO"></span> to go!</h4>
  </div>
</div>

<!-- PERKS MARKUP START -->

<div id="perks" class="content olive">
  <div class="container">
    <h2 class="banner white">Join our farm build!</h2>
    <h5 class="white">Walk along with us as we build this farm. We want transparency to be at the root of our practice, so you know exactly what your contribution goes towards. We have collectively invested $60,000 and would love the farm to be equally supported by the community. By investing, you become a part of our farm whānau.<br>This means:</h5>
    <ul>
      <li class="white">You can opt in to receive regular updates about the farm build, and/or sign up to monthly newsletter</li>
      <li class="white">You will be invited to our yearly Farm Whānau Day. It will be a chance for you to visit, see how your support has helped, take a farm tour and enjoy delicious tasters of our farm’s seasonal produce. It will be a celebratory gathering complete with music and possibly even lawn games!</li>
    </ul>

    <div id="OPTION_RAFFLE" data-id="<?php echo $options['RAFFLE']['id']; ?>" data-link="<?php echo $options['RAFFLE']['stripe_link']; ?>" class="option">
      <div class="cover"><h4>All gone sorry</h4></div>
      <div class="row">
        <div class="four columns">
          <div class="option-img">
            <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="assets/placeholder.png" alt="">
          </div>
        </div>
        <div class="eight columns">
          <h4 class="option-title">Be in to win! <span class="option-price">$30</span></h4>
          <p class="option-description">Who doesn't like a raffle! Get your name in the box to win a box of goodies, your contribution will go towards the big infrastructure on the farm.</p>
          <div class="option-perks">
            <label><b>Adoption Perks</b></label>
            <ul>
              <li>Be in the draw to win a suprise box of Vagabond Veg goodies</li>
            </ul>
          </div>
          <button type="button" class="option-button" name="abopt_single_open" onclick="openModal('RAFFLE')">Invest Now&nbsp;&nbsp;&nbsp;&nbsp;<i class="fad fa-chevron-right"></i></button>
          <button type="button" class="hollow olive"><?php echo $options['RAFFLE']['remaining'] ?> names in the box so far</button>
        </div>
      </div>
    </div>

    <div id="OPTION_TREE" data-id="<?php echo $options['TREE']['id']; ?>" data-link="<?php echo $options['TREE']['stripe_link']; ?>" class="option <?php echo $options['TREE']['remaining'] > 0 ? '' : 'disabled'; ?>">
      <div class="cover"><h4>All gone sorry</h4></div>
      <div class="row">
        <div class="four columns">
          <div class="option-img">
            <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="assets/placeholder.png" alt="">
          </div>
        </div>
        <div class="eight columns">
          <h4 class="option-title">Adopt-a-Tree <span class="option-price">$50</span></h4>
          <p class="option-description">Blow me down, what a gust! We need a shelterbelt to protect our farm from the wild westerly and chilly southerly. Your contribution will go towards planting a green belt of trees and shrubs, made up of natives and exotics. Wind protection, bird homes, bee food, and food forest all in one!</p>
          <div class="option-perks">
            <label><b>Adoption Perks</b></label>
            <ul>
              <li>An invitation to join us on our yearly Farm Whānau Day</li>
            </ul>
          </div>
          <button type="button" class="option-button" name="abopt_single_open" onclick="openModal('TREE')">Invest Now&nbsp;&nbsp;&nbsp;&nbsp;<i class="fad fa-chevron-right"></i></button>
          <button type="button" class="hollow olive">Unlimited Avaliable</button>
        </div>
      </div>
    </div>

    <div id="OPTION_MINI_CHILLER" data-id="<?php echo $options['MINI_CHILLER']['id']; ?>" data-link="<?php echo $options['MINI_CHILLER']['stripe_link']; ?>" class="option <?php echo $options['MINI_CHILLER']['remaining'] > 0 ? '' : 'disabled'; ?>">
      <div class="cover"><h4>All gone sorry</h4></div>
      <div class="row">
        <div class="four columns">
          <div class="option-img">
            <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="assets/placeholder.png" alt="">
          </div>
        </div>
        <div class="eight columns">
          <h4 class="option-title">Adopt-the-Chiller <span class="option-price">$100</span></h4>
          <p class="option-description">Veggies need to chill out! We are buying/building a chiller to keep your produce fresh. Your contribution will go towards the chiller, the concrete pad for the chiller to sit on, and a structure protecting it from the elements.</p>
          <div class="option-perks">
            <label><b>Adoption Perks</b></label>
            <ul>
              <li>Your name on the chiller</li>
              <li>Some Vagabond Vege merch</li>
              <li>An invitation to join us on our yearly Farm Whānau Day</li>
            </ul>
          </div>
          <button type="button" class="option-button" name="abopt_single_open" onclick="openModal('MINI_CHILLER')">Invest Now&nbsp;&nbsp;&nbsp;&nbsp;<i class="fad fa-chevron-right"></i></button>
          <button type="button" class="hollow olive"><?php echo $options['MINI_CHILLER']['remaining'] ?> Remaining</button>
        </div>
      </div>
    </div>

    <div id="OPTION_SINGLE_BED" data-id="<?php echo $options['SINGLE_BED']['id']; ?>" data-link="<?php echo $options['SINGLE_BED']['stripe_link']; ?>" class="option <?php echo $options['SINGLE_BED']['remaining'] > 0 ? '' : 'disabled'; ?>">
      <div class="cover"><h4>All gone sorry</h4></div>
      <div class="row">
        <div class="four columns">
          <div class="option-img">
            <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="assets/placeholder.png" alt="">
          </div>
        </div>
        <div class="eight columns">
          <h4 class="option-title">Adopt-a-Bed <span class="option-price">$150</span></h4>
          <p class="option-description">The farm’s building blocks, the humble vegetable bed! Have peace of mind knowing that there is a soily spot growing goodness in your honour. With diversity comes strength and many hands make light work - so why not be a bed adopter and help us get off (or in?!) the ground.</p>
          <div class="option-perks">
            <label><b>Adoption Perks</b></label>
            <ul>
              <li>A bed named by you</li>
              <li>A surprise of fresh produce from your beds first harvest *</li>
              <li>An invitation to join us on our yearly Farm Whānau Day</li>
            </ul>
          </div>
          <button type="button" class="option-button" name="abopt_single_open" onclick="openModal('SINGLE_BED')">Invest Now&nbsp;&nbsp;&nbsp;&nbsp;<i class="fad fa-chevron-right"></i></button>
          <button type="button" class="hollow olive"><?php echo $options['SINGLE_BED']['remaining'] ?> Remaining</button>
        </div>
      </div>
    </div>

    <div id="OPTION_WASH_PACK" data-id="<?php echo $options['WASH_PACK']['id']; ?>" data-link="<?php echo $options['WASH_PACK']['stripe_link']; ?>" class="option <?php echo $options['WASH_PACK']['remaining'] > 0 ? '' : 'disabled'; ?>">
      <div class="cover"><h4>All gone sorry</h4></div>
      <div class="row">
        <div class="four columns">
          <div class="option-img">
            <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="assets/placeholder.png" alt="">
          </div>
        </div>
        <div class="eight columns">
          <h4 class="option-title">Adopt-the-Wash/Pack <span class="option-price">$250</span></h4>
          <p class="option-description">Quick, the van is pulling up! Would you give us a hand with the clean up?! Your contribution will go towards the processing facilities we need. Help the vege get clean and all dressed up for markets and deliveries!</p>
          <div class="option-perks">
            <label><b>Adoption Perks</b></label>
            <ul>
              <li>Naming rights to your chosen wash/pack equipment piece eg bath bubbler, spray hose, salad spinner washing machine</li>
              <li>A box of sparkly clean, well-dressed vegetables *</li>
              <li>An invitation to join us on our yearly Farm Whānau Day</li>
            </ul>
          </div>
          <button type="button" class="option-button" name="abopt_single_open" onclick="openModal('WASH_PACK')">Invest Now&nbsp;&nbsp;&nbsp;&nbsp;<i class="fad fa-chevron-right"></i></button>
          <button type="button" class="hollow olive"><?php echo $options['WASH_PACK']['remaining'] ?> Remaining</button>
        </div>
      </div>
    </div>

    <div id="OPTION_CHILLER" data-id="<?php echo $options['CHILLER']['id']; ?>" data-link="<?php echo $options['CHILLER']['stripe_link']; ?>" class="option <?php echo $options['CHILLER']['remaining'] > 0 ? '' : 'disabled'; ?>">
      <div class="cover"><h4>All gone sorry</h4></div>
      <div class="row">
        <div class="four columns">
          <div class="option-img">
            <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="assets/placeholder.png" alt="">
          </div>
        </div>
        <div class="eight columns">
          <h4 class="option-title">Adopt-the-Chiller <span class="option-price">$1000</span></h4>
          <p class="option-description">Veggies need to chill out! We are buying/building a chiller to keep your produce fresh. Your contribution will go towards the chiller, the concrete pad for the chiller to sit on, and a structure protecting it from the elements.</p>
          <div class="option-perks">
            <label><b>Adoption Perks</b></label>
            <ul>
              <li>Your name on the chiller</li>
              <li>One box of produce you helped keep fresh *</li>
              <li>An invitation to join us on our yearly Farm Whānau Day</li>
            </ul>
          </div>
          <button type="button" class="option-button" name="abopt_single_open" onclick="openModal('CHILLER')">Invest Now&nbsp;&nbsp;&nbsp;&nbsp;<i class="fad fa-chevron-right"></i></button>
          <button type="button" class="hollow olive"><?php echo $options['CHILLER']['remaining'] ?> Remaining</button>
        </div>
      </div>
    </div>

    <div id="OPTION_TRACTOR_UPGRADE" data-id="<?php echo $options['TRACTOR_UPGRADE']['id']; ?>" data-link="<?php echo $options['TRACTOR_UPGRADE']['stripe_link']; ?>" class="option <?php echo $options['TRACTOR_UPGRADE']['remaining'] > 0 ? '' : 'disabled'; ?>">
      <div class="cover"><h4>All gone sorry</h4></div>
      <div class="row">
        <div class="four columns">
          <div class="option-img">
            <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="assets/placeholder.png" alt="">
          </div>
        </div>
        <div class="eight columns">
          <h4 class="option-title">Upgrade-Danny-the-Tractor <span class="option-price">$4000</span></h4>
          <p class="option-description">The tractor is named, but now it needs personality - add a personalised feature of your choice. Help us make Danny the tractor the best tractor for the job! With your help we can get a fit for purpose tractor with all the implements we need. Fulfil your childhood dream and ours... support our tractor!</p>
          <div class="option-perks">
            <label><b>Adoption Perks</b></label>
            <ul>
              <li>Personalise Danny and name its future home!</li>
              <li>A box of vegetables themed to the tractor’s colour *</li>
              <li>Rides on the tractor by appointment</li>
              <li>10% discount on farm produce for the summer growing season (ending 28/02/22)</li>
              <li>An invitation to join us on our yearly Farm Whānau Day</li>
            </ul>
          </div>
          <button type="button" class="option-button" name="abopt_single_open" onclick="openModal('TRACTOR_UPGRADE')">Invest Now&nbsp;&nbsp;&nbsp;&nbsp;<i class="fad fa-chevron-right"></i></button>
          <button type="button" class="hollow olive"><?php echo $options['TRACTOR_UPGRADE']['remaining'] ?> Remaining</button>
        </div>
      </div>
    </div>

    <div id="OPTION_DOUBLE_BED" data-id="<?php echo $options['DOUBLE_BED']['id']; ?>" data-link="<?php echo $options['DOUBLE_BED']['stripe_link']; ?>" class="option <?php echo $options['DOUBLE_BED']['remaining'] > 0 ? '' : 'disabled'; ?>">
      <div class="cover"><h4>All gone sorry</h4></div>
      <div class="row">
        <div class="four columns">
          <div class="option-img">
            <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="assets/placeholder.png" alt="">
          </div>
        </div>
        <div class="eight columns">
          <h4 class="option-title">Adopt-Two-Beds <span class="option-price">$300</span></h4>
          <p class="option-description">Cute! Snuggle up in your double bed! Get your beds side by side and have them grow together for ever more, bound together in delicious matrimony. Want to show someone you love them? What says ‘I appreciate you’ more than two plots of soil growing life together! Just think of those mycorrhizal fungi intermingling down below...</p>
          <div class="option-perks">
            <label><b>Adoption Perks</b></label>
            <ul>
              <li>Two beds named by you</li>
              <li>A surprise selection of produce from your beds during their first growing season *</li>
              <li>An invitation to join us on our yearly Farm Whānau Day</li>
            </ul>
          </div>
          <button type="button" class="option-button" name="abopt_single_open" onclick="openModal('DOUBLE_BED')">Invest Now&nbsp;&nbsp;&nbsp;&nbsp;<i class="fad fa-chevron-right"></i></button>
          <button type="button" class="hollow olive"><?php echo $options['DOUBLE_BED']['remaining'] ?> Remaining</button>
        </div>
      </div>
    </div>

    <div id="OPTION_FAMILY_BLOCK" data-id="<?php echo $options['FAMILY_BLOCK']['id']; ?>" data-link="<?php echo $options['FAMILY_BLOCK']['stripe_link']; ?>" class="option <?php echo $options['FAMILY_BLOCK']['remaining'] > 0 ? '' : 'disabled'; ?>">
      <div class="cover"><h4>All gone sorry</h4></div>
      <div class="row">
        <div class="four columns">
          <div class="option-img">
            <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="assets/placeholder.png" alt="">
          </div>
        </div>
        <div class="eight columns">
          <h4 class="option-title">Adopt-a-Family-Block <span class="option-price">$500</span></h4>
          <p class="option-description">Want to know your family has a spot in the country? Whatever the make up of your family group, know that there is a bed here for each of you! With roots chatting away underground, here is something for the whole family.</p>
          <div class="option-perks">
            <label><b>Adoption Perks</b></label>
            <ul>
              <li>Four beds named by your family</li>
              <li>A surprise selection of produce from your beds during their first growing season *</li>
              <li>An invitation to join us on our yearly Farm Whānau Day</li>
            </ul>
          </div>
          <button type="button" class="option-button" name="abopt_single_open" onclick="openModal('FAMILY_BLOCK')">Invest Now&nbsp;&nbsp;&nbsp;&nbsp;<i class="fad fa-chevron-right"></i></button>
          <button type="button" class="hollow olive"><?php echo $options['FAMILY_BLOCK']['remaining'] ?> Remaining</button>
        </div>
      </div>
    </div>

    <div id="OPTION_BLOCK_BREAK" data-id="<?php echo $options['BLOCK_BREAK']['id']; ?>" data-link="<?php echo $options['BLOCK_BREAK']['stripe_link']; ?>" class="option <?php echo $options['BLOCK_BREAK']['remaining'] > 0 ? '' : 'disabled'; ?>">
      <div class="cover"><h4>All gone sorry</h4></div>
      <div class="row">
        <div class="four columns">
          <div class="option-img">
            <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="assets/placeholder.png" alt="">
          </div>
        </div>
        <div class="eight columns">
          <h4 class="option-title">Adopt-a-Block-Break<span class="option-price">$1000</span></h4>
          <p class="option-description">This place is windy! We need windbreaks to protect our beds from the strong prevailing wind. Would you like to contribute to windbreaks between our beds? We need 6 breaks going up on the main production fields. Your contribution will go towards the materials needed for one of these breaks, and a selection of shrubs for planting shelterbelts.</p>
          <div class="option-perks">
            <label><b>Adoption Perks</b></label>
            <ul>
              <li>You get to name your windbreak</li>
              <li>One box of fresh produce from the beds you helped protect from the wind *</li>
              <li>An invitation to join us on our yearly Farm Whānau Day</li>
            </ul>
          </div>
          <button type="button" class="option-button" name="abopt_single_open" onclick="openModal('BLOCK_BREAK')">Invest Now&nbsp;&nbsp;&nbsp;&nbsp;<i class="fad fa-chevron-right"></i></button>
          <button type="button" class="hollow olive"><?php echo $options['BLOCK_BREAK']['remaining'] ?> Remaining</button>
        </div>
      </div>
    </div>

    <div id="OPTION_HALF_TUNNEL" data-id="<?php echo $options['HALF_TUNNEL']['id']; ?>" data-link="<?php echo $options['HALF_TUNNEL']['stripe_link']; ?>" class="option <?php echo $options['HALF_TUNNEL']['remaining'] > 0 ? '' : 'disabled'; ?>">
      <div class="cover"><h4>All gone sorry</h4></div>
      <div class="row">
        <div class="four columns">
          <div class="option-img">
            <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="assets/placeholder.png" alt="">
          </div>
        </div>
        <div class="eight columns">
          <h4 class="option-title">Adopt-the-Seed-House <span class="option-price">$7000</span></h4>
          <p class="option-description">For the calm, nurturing individual who perhaps enjoys quiet music and slow walks in the bush, the propagation tunnel is for you! It’s the seeds first entry into the world, where they receive tender care to grow strong before they go out into the beds. Give the plants the growth they deserve and support us with half a propagation tunnel!</p>
          <div class="option-perks">
            <label><b>Adoption Perks</b></label>
            <ul>
              <li>Our seedling house named by you</li>
              <li>A graduation box of produce who have made it through the whole life cycle *</li>
              <li>10% discount on farm produce for the summer growing season (ending 28/02/22)</li>
              <li>An invitation to join us on our yearly Farm Whānau Day</li>
            </ul>
          </div>
          <button type="button" class="option-button" name="abopt_single_open" onclick="openModal('HALF_TUNNEL')">Invest Now&nbsp;&nbsp;&nbsp;&nbsp;<i class="fad fa-chevron-right"></i></button>
          <button type="button" class="hollow olive"><?php echo $options['HALF_TUNNEL']['remaining'] ?> Remaining</button>
        </div>
      </div>
    </div>

    <div id="OPTION_WHOLE_TUNNEL" data-id="<?php echo $options['WHOLE_TUNNEL']['id']; ?>" data-link="<?php echo $options['WHOLE_TUNNEL']['stripe_link']; ?>" class="option <?php echo $options['WHOLE_TUNNEL']['remaining'] > 0 ? '' : 'disabled'; ?>">
      <div class="cover"><h4>All gone sorry</h4></div>
      <div class="row">
        <div class="four columns">
          <div class="option-img">
            <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="assets/placeholder.png" alt="">
          </div>
        </div>
        <div class="eight columns">
          <h4 class="option-title">Adopt-the-Tunnel <span class="option-price">$7000</span></h4>
          <p class="option-description">For the party person, the growing tunnel is filled with heat and abundance; think tomatoes, eggplants and capsicum pumping with fruit! Give the plants the party they deserve and support us with a whole growing tunnel.</p>
          <div class="option-perks">
            <label><b>Adoption Perks</b></label>
            <ul>
              <li>An entire tunnel named by you</li>
              <li>A party box of delicious produce from your tunnel *</li>
              <li>10% discount on farm produce for the summer growing season (ending 28/02/22)</li>
              <li>An invitation to join us on our yearly Farm Whānau Day</li>
            </ul>
          </div>
          <button type="button" class="option-button" name="abopt_single_open" onclick="openModal('WHOLE_TUNNEL')">Invest Now&nbsp;&nbsp;&nbsp;&nbsp;<i class="fad fa-chevron-right"></i></button>
          <button type="button" class="hollow olive"><?php echo $options['WHOLE_TUNNEL']['remaining'] ?> Remaining</button>
        </div>
      </div>
    </div>

    <div id="OPTION_TRACTOR" data-id="<?php echo $options['TRACTOR']['id']; ?>" data-link="<?php echo $options['TRACTOR']['stripe_link']; ?>" class="option <?php echo $options['TRACTOR']['remaining'] > 0 ? '' : 'disabled'; ?>">
      <div class="cover"><h4>All gone sorry</h4></div>
      <div class="row">
        <div class="four columns">
          <div class="option-img">
            <img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="assets/placeholder.png" alt="">
          </div>
        </div>
        <div class="eight columns">
          <h4 class="option-title">Adopt-the-Tractor <span class="option-price">$5000</span></h4>
          <p class="option-description">Help us with our heavy lifting! This human scale farm needs a little mechanical help from time to time - mowing, moving compost, rocks and wood chip etc. Fulfil your childhood dream and ours... support us with a tractor!</p>
          <div class="option-perks">
            <label><b>Adoption Perks</b></label>
            <ul>
              <li>Our tractor, named by you!</li>
              <li>A box of vegetables themed to the tractor’s colour *</li>
              <li>Rides on the tractor by appointment</li>
              <li>10% discount on farm produce for the summer growing season (ending 28/02/22)</li>
              <li>An invitation to join us on our yearly Farm Whānau Day</li>
            </ul>
          </div>
          <button type="button" class="option-button" name="abopt_single_open" onclick="openModal('TRACTOR')">Invest Now&nbsp;&nbsp;&nbsp;&nbsp;<i class="fad fa-chevron-right"></i></button>
          <button type="button" class="hollow olive"><?php echo $options['TRACTOR']['remaining'] ?> Remaining</button>
        </div>
      </div>
    </div>

    <p class="white">* Vegetables perks are only deliverable to the Wellington/Wairarapa region, or by direct pick-up from the farm. For other regions your vegetables will be replaced by a non-perishable perk</p>
  </div>
</div>

<div id="breakdown" class="content white">
  <div class="container">
    <h4 class="banner olive">To date we have spent around $35,000 on items such as compost, propagation equipment, seeds, infrastructure, woodchip, irrigation, insurance, tools and leasing the land. Below are the remaining minimum start-up costs.</h4>

    <ol>
      <li class="olive">Commercial tunnel houses ($14,000)</li>
      <li class="olive">Commercial chiller ($8000)</li>
      <li class="olive">Compost for permanent beds ($7000)</li>
      <li class="olive">Building a wash/pack facility ($7000)</li>
      <li class="olive">Installing & planting windbreaks ($6000)</li>
      <li class="olive">Planting Shelterbelts ($2500)</li>
      <li class="olive">Small second hand tractor ($10,000)</li>
    </ol>

    <p class="olive"><b>Stretch Goal:</b> $80,000 covers increased costs within our wash/pack processing facilities and the purchase of holding tanks for water resilience. A large investor has pushed us past our ultimate goal and allowed us to add a stretch goal to cover increased costs!</p>
    <p class="olive"><b>Ideal Goal:</b> $60,000 matches our personal investment in the farm. This gives us full financial stability and allows us to invest in the quality infrastructure we need now. Reaching our ultimate goal allows us to spend more time focussed on growing our farm, setting up our CSA membership and supporting more positive change within our food systems.</p>
    <p class="olive"><b>Minimum Goal:</b> $40,000 allows us to purchase the basics for our farm set up. Our minimum goal means we may have to wait until we have a steady income stream in order to invest in the quality infrastructure we need now.</p>

  </div>
</div>

<footer>
  <h5>Vagabond Vege, Monty's Lane, Te Hupenui, Greytown</h5>
  <h5><a href="https://www.facebook.com/vagabond.vege" target="_blannk">Facebook</a><span>|</span><a href="https://www.instagram.com/vagabond.vege" target="_blank">Instagram</a></h5>
</footer>
