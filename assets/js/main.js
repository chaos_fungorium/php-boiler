// proto function to capatalize text
String.prototype.capatalize = function() {
  return this.substr(0, 1).toUpperCase() + this.substr(1, this.length-1);
}
// */ Helper functions /*
function centerCSS(ev) {
  return {top:ev.pageY+'px',left:ev.pageX+20+'px'};
}

// BEGIN
jQuery(document).ready(function($) {
  //bLazy starts the lazy load watcher on images (boosts page speed)
  var bLazy = new Blazy({loadInvisible:true});
  //get the timer set correct
  let to_go = moment("2021-12-20 22:00:00").toNow(moment());
  $("#TO_GO").text(to_go.capatalize());
});

//MENU OPEN AND ANIMATE
function toggleMenu() {
  if (window.menu_open) {
    $("#menu nav").fadeOut();
    $("#menu .social").fadeOut();
    setTimeout(function () {
      $("#menu nav li").hide();
      $("#menu_back").removeClass('open');
    }, 400);
    setTimeout(function () {
      $("#menu").hide();
    }, 1200);
    window.menu_open = false;
  } else {
    $("#menu").show();
    $("#menu_back").addClass('open');
    setTimeout(function () {
      $("#menu .social").fadeIn();
      $("#menu nav").show();
      let i = 0;
      $("#menu nav li").each(function (i, el) {
        setTimeout(function () { $(el).fadeIn() }, 100*i);
        i++;
      })
    }, 550);
    window.menu_open = true;
  }
}

//CROWDFUND FORM SUBMIT
//THIS FUNCTION TAKES OVER FORM SUBMITION TO SERVER

function submitForm() {
  //gather information
  let name = $("#form_name").val();
  let email = $("#form_email").val();
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  let invalid = false;

  //test the email on regex
  // TODO: Check regex for all emails
  if (!re.test(email)) {
    $("#form_email ~ label.warning").addClass('shown');
    invalid = true;
  } else {
    $("#form_email ~ label.warning").removeClass('shown');
  }
  //test name on length
  if (!name || name.length < 3) {
    $("#form_name ~ label.warning").addClass('shown');
    invalid = true;
  } else {
    $("#form_name ~ label.warning").removeClass('shown');
  }

  if (invalid) {
    return;
  }

  //on success, begin UI change
  $("#formLoader").fadeIn();
  $("#formPlatform").hide();
  //prepare success UI
  $("#particular").text($("#form_name").val());

  //POST SUBMIT
  window.FORM_COMPLETE = true;
  $.post('/submit_donate', $('form#donate_form').serialize(), function(data, response) {
    if (data === 'SUCCESS') {

    } else {
      alert('An error occured. Please try again soon.');
    }
  });

  setTimeout(() => {
    $("#formLoader").hide();
    $("#formSuccess").fadeIn();
  }, 2000);
}

//CROWDFUND OPTION SELECTED
//SET UP AND DISPLAY THE MODAL

function openModal(OPTION_NAME) {
  var id = "OPTION_"+OPTION_NAME; //DOM id used to store the option
  var title = $("#"+id+" .option-title").text(); //copy the name
  var description = $("#"+id+" .option-description").text(); //copy the description
  var dind = title.indexOf("$");
  var price = title.substr(dind, title.length); //get the price
  var option_id = $("#"+id).attr('data-id'); //get the SQL option id
  var link = $("#"+id).attr('data-link'); //get the Stripe Link

  //MOVE DETAILS TO THE DOM
  $("#FINAL_TITLE").text(title);
  $("#perk_name").val(title);
  $("#FINAL_DESCRIPTION").text(description);
  $("#donate_id").val(option_id);
  $("#donate_link").val(link);
  $("#donate_option").val(OPTION_NAME);
  $("#donate_dollars").val(price.replace("$", ""));
  $("button#confirm").text("Save & Pay "+price);
  $("#STRIPE_LEAD_ON").attr('href', link);

  //DISPLAY THE DATA
  $("#donateModal").fadeIn();
}

function closeModal() {
  //FADE OUT AND CLEAR MODAL
  $("#donateModal").fadeOut();
  if (window.FORM_COMPLETE) {
    $("#formLoader").hide();
    $("#formSuccess").hide();
    $("#formPlatform").show();
    $('form').trigger('reset');
  }
}
