**Crowdfunding Boiler Plate**

*Requirments*

1. PHP 7.3.* (NB Heroku runs 7.3.29).
2. Composer.
3. MySQL.
4. FlightPHP ^1.3 (included in Composer file).
5. PHPMailer ^6.5 (included in Composer file).
6. Smarty ^3.1 (included in Composer file).
7. Gmail Account.

---

## Update the following in MarkUp

1. Page Titles
2. Design, colors and fonts - currently using Vagabond Vege brand design
3. All img src
4. Google Analytics code (in layout.php)
5. src path of built css and js (in layout.php)
6. Descriptions of options in support.php

---

## Set up SQL tables

**Create the `donations` table**

This table stores all of the entries into the crowd fund

*CREATE TABLE `donations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `donation_option` varchar(255) DEFAULT NULL,
  `donation_amount` int(11) DEFAULT NULL,
  `wants_updates` tinyint(1) DEFAULT NULL,
  `wants_newsletter` tinyint(1) DEFAULT NULL,
  `added_to_newsletter` tinyint(1) DEFAULT NULL,
  `added_to_updated` tinyint(1) DEFAULT NULL,
  `donation_recieved` tinyint(1) DEFAULT NULL,
  `date_submitted` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3026 DEFAULT CHARSET=utf8;*

**Create the `options` table**

This table stores all the options, the stripe link, and the remaining amount

*CREATE TABLE `options` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(256) DEFAULT NULL,
  `remaining` int(11) DEFAULT NULL,
  `stripe_link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;*

---

## Option settings in MySQL

You need to manually enter the crowd funding options in MySQL

1. **id** int(11) UNIQUE -- use default or custom
2. **option_name** varchar(256) UNIQUE -- use uppercase simple description, this option is used in the DOM, e.g. SINGLE_BED
3. **remainging** int(11) -- the number of remaining options
4. **stripe_link** varchar(256) -- the URL to the Stripe Payment Link

---

## Update the individual DOM elements for the fund options

Make sure to create a DOM element for each fund option in the `support.php` file

e.g.

	<div id="OPTION_TREE `the option id`" data-id="<?php echo $options['TREE']['id']; ?>" data-link="<?php echo $options['TREE']['stripe_link']; ?>" class="option <?php echo $options['TREE']['remaining'] > 0 ? '' : 'disabled'; ?>">
		<div class="cover"><h4>All gone sorry</h4></div>`this shows when all are gone`
		<div class="row">
			<div class="four columns">
				<div class="option-img">
					<img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="assets/placeholder.png" alt=""> `auto loading image`
				</div>
			</div>
			<div class="eight columns">
				`enter the option title`
				<h4 class="option-title">Adopt-a-Tree <span class="option-price">$50</span></h4>
				`option description`
				<p class="option-description">Blow me down, what a gust! We need a shelterbelt to protect our farm from the wild westerly and chilly southerly. Your contribution will go towards planting a green belt of trees and shrubs, made up of natives and exotics. Wind protection, bird homes, bee food, and food forest all in one!</p>
				`option perks`
				<div class="option-perks">
					<label><b>Adoption Perks</b></label>
					<ul>
						<li>An invitation to join us on our yearly Farm WhÄnau Day</li>
						<li>.....</li>
					</ul>
				</div>
				`change openModal attr to suit the option and id`
				<button type="button" class="option-button" name="abopt_single_open" onclick="openModal('TREE')">Invest Now&nbsp;&nbsp;&nbsp;&nbsp;<i class="fad fa-chevron-right"></i></button>
				<button type="button" class="hollow olive">Unlimited Avaliable</button>
			</div>
		</div>
	</div>
