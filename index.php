<?php

  require 'vendor/autoload.php';

  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\Exception;

  $smarty = new Smarty();

  //restart any current browser sessions
	session_start();
  if (!$_SESSION['user_defined_admin_in']) {
    $_SESSION['user_defined_admin_in'] = 'NO';
  }

  //IMPORT VARIABLES FROM ENV
  define('DB_USER'      , getenv('DATABASE_USER'));
	define('DB_PASS'      , getenv('DATABASE_PASSWORD'));
	define('DB_NAME'      , getenv('DATABASE_NAME'));
	define('DB_HOST'      , getenv('DATABASE_HOST'));
  define('ADMIN_PASS'   , getenv('ADMIN_PASSWORD'));
  define('EMAIL_PASS'   , getenv('EMAIL_PASSWORD'));
  define('EMAIL_ADDRESS', getenv('EMAIL_ADDRESS'));

  //connect to MySQL
  $mysqli = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
  if (mysqli_connect_errno($mysqli)) {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

  //set the flight views path
  Flight::set('flight.views.path', 'views/');

  Flight::route('/', function(){

    global $mysqli;
    $query = "SELECT SUM(donation_amount) as TotalDonation from donations where 1;";
    $res = mysqli_query($mysqli, $query);
    $row = mysqli_fetch_assoc($res);
    $total_donation = (int)$row["TotalDonation"];

    // NOTE: CHANGE TO YOUR VALUES (MAX GOALS)
    if ($total_donation < 60000) { //min goal
      $percentage = (int)(($total_donation / 60000)*100);
      $progress = (int)(($total_donation / 60000)*100);
    } else { //stretch goal
      $percentage = (int)(($total_donation / 80000)*100);
      $progress = (int)(($total_donation / 80000)*100);
    }

    $query = "SELECT COUNT(id) as TotalDonations FROM donations;";
    $res = mysqli_query($mysqli, $query);
    $row = mysqli_fetch_assoc($res);
    $total_donations = (int)$row["TotalDonations"];

    $query = "SELECT * FROM options WHERE 1";
    $res = mysqli_query($mysqli, $query);
    $options = [];
    while ($row = mysqli_fetch_assoc($res)) {
      $options[$row['option_name']] = $row;
    }

    Flight::render('support', array(
      'total_donation' => $total_donation,
      'percentage' => $percentage,
      'progress' => $progress,
      'options' => $options,
      'total_donations' => $total_donations
    ), 'body_content');
    Flight::render('layout', array('title' => 'PAGE TITLE'));
  });

  Flight::route('POST /submit_donate', function(){
    global $mysqli;
    $request = Flight::request();
    $form_data = $request->data;

    $FIELDS = ['name','email','region','donation_option','donation_amount','wants_newsletter','wants_updates'];
    $BOOLEAN_FIELD = ['wants_newsletter', 'wants_updates'];

    $search = [';', '%', '*', "'", '"'];
    $replace = ['\;', '\%', '\*', "\'", '\"'];

    $QUERY_VALUES = "";

    foreach ($FIELDS as $INDEX => $FIELD_NAME) {
      if (array_search($FIELD_NAME, $BOOLEAN_FIELD, TRUE) !== FALSE) {
        if ($form_data[$FIELD_NAME] !== NULL) {
          $NEXT_QUERY_VAL = ($form_data[$FIELD_NAME] === "YES") ? 1 : 0;
        } else {
          $NEXT_QUERY_VAL = 0;
        }
      } else {
        if ($form_data[$FIELD_NAME] !== NULL) {
          $NEXT_QUERY_VAL = $form_data[$FIELD_NAME];
        } else {
          $NEXT_QUERY_VAL = "";
        }
      }

      $QUERY_VALUES .= "'" . str_replace($search, $replace, $NEXT_QUERY_VAL) . "', ";
    }
    $QUERY_VALUES = substr($QUERY_VALUES, 0, strlen($QUERY_VALUES)-2);

    $query = "INSERT INTO donations (" . implode(",", $FIELDS) . ") VALUES (" . $QUERY_VALUES . ");";
    $res = mysqli_query($mysqli, $query);

    if ($form_data['donation_option'] === 'RAFFLE') {
      $query = "UPDATE options SET remaining = remaining + 1 WHERE id = ".$form_data['donate_id'];
      $option_res = mysqli_query($mysqli, $query);
    } else {
      $query = "UPDATE options SET remaining = remaining - 1 WHERE id = ".$form_data['donate_id'];
      $option_res = mysqli_query($mysqli, $query);
    }

    $sendResponse = sendEmail($form_data['name'], $form_data['email'], $form_data['donate_link'], $form_data['donation_amount'], $form_data['perk_name']);

    if ($res && $sendResponse) {
      Flight::halt(200, 'SUCCESS');
      exit;
    } else {
      Flight::halt(200, 'ERROR');
      exit;
    }
  });

  Flight::route('GET /manage', function () {
    global $mysqli;
    if ($_SESSION['user_defined_admin_in'] == 'confirmed') {
      $query = "SELECT * FROM follow_up WHERE 1";
      $res = mysqli_query($mysqli, $query);
      $follow_ups = [];
      while ($row = mysqli_fetch_assoc($res)) {
        array_push($follow_ups, $row);
      }

      $query = "SELECT * FROM follow_up WHERE hasSent = 0";
      $res = mysqli_query($mysqli, $query);
      $not_sent = [];
      $not_sent_ids = "[";
      while ($row = mysqli_fetch_assoc($res)) {
        array_push($not_sent, $row);
        $not_sent_ids .= "'".$row['access']."',";
      }
      $not_sent_ids .= "]";

      Flight::render('admin_view', array('follow_ups' => $follow_ups, 'ns_ids' => $not_sent_ids), 'body_content');
      Flight::render('layout', array('title' => 'Admin'));
    } else {
      Flight::render('admin_login', array(), 'body_content');
      Flight::render('layout', array('title' => 'Admin Login'));
    }
  });
  Flight::route('POST /manage', function () {
    $request = Flight::request();
    $form_data = $request->data;
    $password = $form_data->password;
    if ($password == ADMIN_PASS) {
      $_SESSION['user_defined_admin_in'] = 'confirmed';
      Flight::redirect('/manage');
    } else {
      die('permission denied');
    }
  });

  function sendEmail($name, $email, $link, $amount, $perk) {
    global $smarty;
    $mail = new PHPMailer(true);
    try {
      //Server settings
      $mail->SMTPDebug = 2;
      $mail->isSMTP();
      $mail->Host = 'smtp.gmail.com';
      $mail->SMTPAuth = true;
      $mail->Username = EMAIL_ADDRESS;
      $mail->Password = EMAIL_PASS;
      $mail->SMTPSecure = 'tls';
      $mail->Port = 587;

      //Recipients
      $mail->setFrom(EMAIL_ADDRESS, 'Vagabond Vege');
      $mail->addAddress($email, $name);

      $mail->isHTML(true);
      $mail->Subject = 'Thanks for Investing in Vagabond Vege';

      $smarty->assign('name', $name);
      $smarty->assign('perk', $perk);
      $smarty->assign('amount', $amount);
      $smarty->assign('payLink', $link);
      $body = $smarty->fetch('./views/email.tpl');

      $mail->Body    = $body;
      $mail->send();
      return true;
    } catch (Exception $e) {
      return $mail->ErrorInfo;
      return false;
    }
  }

  Flight::start();

?>
